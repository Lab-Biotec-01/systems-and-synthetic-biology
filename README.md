# Systems and Synthetic Biology

This repository contains material from the course Systems and Synthetic Biology provided at Wageningen UR. The material here accompanies the publication:

## Overview

A calendar overview of the course is provided in the file: Course_Schedule_2021.xlsx

This shows how the course is split between lectures, practicals, and tutorials. In weeks 1-5 lectures are given twice a week, computational/laboratory practicals twice a week, plus an extra self-study period or tutorial. In weeks 6 and 7, students then complete a short project before sitting an exam in week 8. The solutions to the project and the exam are not provided. Please see the publication for extra details.

We have also provided installation instructions for course software (Installation_Instructions.docx), instructions for a discussion on published research (paper_debate_instructions.pdf) and the raw data from the course evaluation in the 2021/22 academic year (course_survey.pdf).

The material has been provided in the directories as described below.

## Week 1: Introduction

In this week, students complete three tasks:

1. introductory class to Python and Jupyter notebooks: Week1_Pr1_Tuesday{_solution}.ipynb and Week1_Pr1_Tuesday_extra{_solution}.ipynb
2. use of COBRApy for flux balance analysis: Week1_Pr2_Thursday{_solution}.ipynb
3. a self-study exercise on COBRApy: Week1_selfStudy_Friday{_solution}.ipynb

## Week 2: Design

In this week, students complete three tasks:

1. introductory class to iBioSim for model design: Week2_Pr1_Tuesday.pdf and the associated compressed solutions folder
2. use of iBioSim to build a system containing an inducible fluorescent reporter protein: Week2_Pr2_Thursday{_solutions}.pdf and the associated compressed solutions folder
3. a self-study exercise analysing the ODEs obtained from iBioSim: Week2_SelfStudy_Friday{_solutions}.pdf

## Week 3: Learn

In this week, students complete two tasks:

1. introductory class to iBioSim for model design: Week3_Pr1_Tuesday{_solutions}.ipynb
2. use of iBioSim to build a system containing an inducible fluorescent reporter protein: Week3_Pr2_Thursday{_Solutions}.ipynb

## Week 4: Build

In this week, students learn how to build a genetic circuit in the lab. Students need to:

1. learn how to design DNA parts using. We provide here Genbank files exported from Benchling: pC0-GG.gb, pSB1C3-bs1-sg4.gb, pSB1C3-bs4-sg1.gb
2. construct the system within E. coli: week4_protocol.docx

## Week 5: Test

In this week, students will experimentally test their system: week5_protocol.docx

## Short project

The short project brings together the model design and data fitting methods from weeks 1-3 with the experiments conducted in weeks 4-5. We include here a:

1. short project description with hints/tips: Project.pdf
2. an accompanying Jupyter notebook to help students: Project_TemplateNotebook.ipynb

We have not included the solutions here as these are assessed during the course.

## Authors and acknowledgments
Authors: Robert W Smith, Luis Garcia Morales, Vitor AP Martins dos Santos, Edoardo Saccenti

We would like to acknowledge the students that followed the course during the previous academic years and helped us shape the course into its current form. Furthermore, thanks to Pedro Fontanarrosa, Lukas Buecherl and Chris Myers for iBioSim support and advice when developing our models. Finally, we would like to kindly thank our colleagues and lecturers at the Laboratory of Systems and Synthetic Biology (Wageningen University & Research, the Netherlands) and collaborators for discussion on course development, not just for this course but for others in our educational portfolio.

## Reference
If this material is used within your courses then we would, of course, appreciate to be referenced via our publication, found here:
