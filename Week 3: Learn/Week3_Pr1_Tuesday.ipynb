{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Systems & Synthetic Biology\n",
    "## Part 1: Simulating ODEs with python\n",
    "This notebook will guide you through the steps needed to simulate systems of ordinary differential equations in python.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 1: Importing modules\n",
    "First we import the modules we will need later on. We already introduced numpy and matplotlib in the python tutorial. For the actual numerical integration we will use the function **odeint** from the **scipy** module. Since this is the only function in this module that we will use, we will import it directly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#import necessary packages\n",
    "import numpy as np #math and arrays\n",
    "from scipy.integrate import odeint #differential equations\n",
    "import matplotlib.pyplot as plt #plotting"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#run a \"magic\" command to make sure figures are rendered properly in the notebook\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 2: Define a system of differential equations\n",
    "Next, we need to define our system of differential equations. For this example, we will use a simple system with two equations:\n",
    "\\begin{align}\n",
    "\\frac{dx_1}{dt} &= a \\cdot x_1 - b\\cdot x_1\\cdot x_2\\\\\n",
    "\\frac{dx_2}{dt} &= c \\cdot x_1\\cdot x_2 - d \\cdot x_2\n",
    "\\end{align}\n",
    "Here, $x_1$ and $x_2$ are the two state variables and $a$, $b$, $c$, and $d$ are parameters. This system can be thought of as a representation of simple predator prey dynamics, where $x_1$ is the density of the prey and $x_2$ is the density of the predator.\n",
    "\n",
    "The numerical integration function we will use (**odeint**) requires us to define a function that calculates the derivatives of all state variables as specified by our differential equation. Note that we will not be calling this derivative function ourselves; it will be passed on to odeint which will call it internally. The numerical integration performed by odeint is essentially similar to the forward euler scheme explained in the lectures. It will start at an initial condition, calculate the derivatives there and use those to take a small step forward in time. This process is repeated until the specified end time is reached. The main difference with forward euler is that the size of the time steps is chosen automatically the steps are a bit more complicated and more accurate, but the process still relies on repeated evaluations of the derivatives. \n",
    "\n",
    "Therefore, the function for the derivatives of our ODE must therefore be defined in the way that the odeint function expects. \n",
    "It will pass a list of current values for all states to the first argument, the current time point to the second, and a list of parameters you gave it will be passed to the remaining arguments. This means that even though we will almost never use the value of the current time point, we still need to provide a parameter for it in our derivative function, because that is what odeint expects. The expected output is a list of derivatives of all states in the same order."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def deriv(x, t, parameters):\n",
    "    \n",
    "    # Unpack the state variable information\n",
    "    # The first argument contains the values of the state variables in order\n",
    "    x1 = x[0] \n",
    "    x2 = x[1]\n",
    "    \n",
    "    # Unpack the parameter values - similar to the state variables above\n",
    "    a = parameters[0]\n",
    "    b = parameters[1]\n",
    "    c = parameters[2]\n",
    "    d = parameters[3]\n",
    "\n",
    "    # Calculate the derivatives of the two states\n",
    "    dx1dt = a*x1 - b*x1*x2\n",
    "    dx2dt = c*x1*x2 - d*x2\n",
    "    \n",
    "    # Make a list of all derivatives in the correct order\n",
    "    dxdt = [dx1dt, dx2dt]\n",
    "\n",
    "    # Return the derivatives\n",
    "    return dxdt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 3: Set the parameters for the simulation experiment\n",
    "Now, we specify the precise conditions for which we want our ODEs simulated. First, we make a numpy array with all the time steps for which we want our states calculated. (Note that these are not the integration time steps used by odeint. If the integration requires smaller time steps to be accurate, odeint will use more steps internally but provide output only at the steps you specified.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the simulation end time and number of time points for which you want output\n",
    "t_end = 10\n",
    "Nsteps = 200\n",
    "\n",
    "# Make a numpy array with the time steps for which you want output\n",
    "timepoints = np.linspace(0, t_end, Nsteps) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we can specify the values of the parameters we want to use. For now, let's set them all to 1. Parameters need to be provided in a tuple *in the same order* as they appear in the derivatives function we specified above. (Using the same variable names as in the derivatives function is recommended to prevent confusion, but python has no way of linking the variable names you use here to the ones from above, so it will look only at the order in which they are provided.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Specify parameter values\n",
    "a = 1\n",
    "b = 1\n",
    "c = 1\n",
    "d = 1\n",
    "parameters = (a,b,c,d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The last thing we need to do before we can start the actual simulation is defining the initial conditions of the state variables. Again, these need to be provided in the same order that you used in the derivative function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set initial values for all state variables\n",
    "x1_init = 2\n",
    "x2_init = 4\n",
    "\n",
    "x_init = [x1_init, x2_init] #combine into a list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 4: Run the simulation experiment\n",
    "Now we are ready to call the **odeint** function. This function takes three required arguments: the derivative function we defined at the beginning, the list of initial conditions, and the array of time points. If your derivative function needs any parameters, these should be passed in a tuple to an optional argument called **args**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate state variables at requested time points, note here that the ,) in the args term \n",
    "# is required to avoid errors when running\n",
    "x_t = odeint(deriv, x_init, timepoints, args = (parameters,))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The odeint function returns a numpy matrix with a column for each state variable and a row for each time point in the timepoints array, containing the values of these state variables at these time points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Separate results into arrays for each state variable\n",
    "x1_t = x_t[:,0]\n",
    "x2_t = x_t[:,1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Step 5: Plot the results \n",
    "Finally, we can plot the results of the numerical integration using matplotlib."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot the state variables against time\n",
    "plt.plot(timepoints, x1_t, label='x1')\n",
    "plt.plot(timepoints, x2_t, label='x2')\n",
    "plt.legend()\n",
    "plt.xlabel('time')\n",
    "plt.ylabel('states')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Running multiple simulations\n",
    "If you want to run multiple simulations, it may be convenient to wrap the entire simulation code into a function that takes the parameters and initial conditions as input arguments. You can have your function return both the array of timepoints and the simulation results by separating them with a comma."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def simulateODE(parameter_values, initial_conditions, t_end = 10, Nsteps = 200):\n",
    "    # Note here that t_end and Nsteps have been given default values - these values are used if no values are\n",
    "    # specified when using the function\n",
    "\n",
    "    # Make a numpy array with the time steps for which you want output\n",
    "    timepoints = np.linspace(0, t_end, Nsteps)\n",
    "\n",
    "    # Specify parameter values\n",
    "    parameters = parameter_values\n",
    "\n",
    "    # Set initial values for all state variables\n",
    "    x_init = initial_conditions\n",
    "\n",
    "    # Calculate state variables at requested time points\n",
    "    x_t = odeint(deriv, x_init, timepoints, args = (parameters,))\n",
    "    \n",
    "    # Return time points and state variables at those time points\n",
    "    # This technically combines both variables into a tuple which can be unpacked after calling the function\n",
    "    return timepoints, x_t\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The same thing can be done for the plotting code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plotODEoutput(timepoints, x_t):\n",
    "    # Separate results into arrays for each state variable\n",
    "    x1_t = x_t[:,0]\n",
    "    x2_t = x_t[:,1]\n",
    "    \n",
    "    # Plot the state variables against time\n",
    "    plt.plot(timepoints, x1_t, label='x1')\n",
    "    plt.plot(timepoints, x2_t, label='x2')\n",
    "    plt.legend()\n",
    "    plt.xlabel('time')\n",
    "    plt.ylabel('states')\n",
    "    plt.show()\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can then call these functions multiple times for different settings without having to rewrite all that code over and over again. To split the output from the simulation function back into two separate arrays, simply provide two variable names separated by a comma."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Simulations for various parameter values and initial conditions\n",
    "parameters = [1,1,1,1]\n",
    "initial_conditions = [12,10]\n",
    "timepoints, x_t = simulateODE(parameters,initial_conditions, t_end = 60)\n",
    "plotODEoutput(timepoints, x_t)\n",
    "\n",
    "parameters = [1,1,0.1,1]\n",
    "initial_conditions = [2,4]\n",
    "timepoints, x_t = simulateODE(parameters,initial_conditions, t_end = 30)\n",
    "plotODEoutput(timepoints, x_t)\n",
    "\n",
    "parameters = [1,1,0.1,1]\n",
    "initial_conditions = [12,10]\n",
    "timepoints, x_t = simulateODE(parameters,initial_conditions, t_end = 50)\n",
    "plotODEoutput(timepoints, x_t)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Alternatively, a script could be written with a for loop to plot all the states in the output array in a single figure, regardless of how many states there are. To determine the number of states in an output array x_t, you can use the len function on the first row of this array (so Nstates = len(x_t[0,:]) ).\n",
    "\n",
    "For the labels in the legend, this will give some trouble, because you don't know how to label the output of an arbitrary ODE in advance. To solve this problem, you can add an optional argument labels that takes a list of names to use as labels (one for each state) and defaults to an empty list. You can then use a conditional statement to set the label to the element that corresponds with the current state if the labels list is long enough to contain this entry. If not, you can skip the label by feeding the label argument an empty character string (label = '')."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def plotODEoutput_withForLoop(timepoints, x_t, labels = []):\n",
    "    # Determine the number of states in the output array\n",
    "    Nstates = len(x_t[0,:])\n",
    "    \n",
    "    # Loop over the number of states\n",
    "    for ii in range(Nstates):\n",
    "        # Get the time series data of the state at the current index\n",
    "        xii_t = x_t[:,ii]\n",
    "    \n",
    "        # If the labels argument has an element at the current index, use that element as label\n",
    "        if len(labels) > ii:\n",
    "            label = labels[ii]\n",
    "        else: # If not, leave the label empty\n",
    "            label = ''\n",
    "            \n",
    "        # Plot the current state variable against time\n",
    "        plt.plot(timepoints, xii_t, label=label)\n",
    "        \n",
    "    # These statements apply to the entire figure and should be outside the for loop\n",
    "    plt.legend()\n",
    "    plt.xlabel('time')\n",
    "    plt.ylabel('states')\n",
    "    plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# We can use this script and you should get the same plot as one of those obtained above\n",
    "plotODEoutput_withForLoop(timepoints, x_t, labels = ['x_1','x_2'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Part 2: Coding the iBioSim model\n",
    "We are going to focus here on the ODEs of the simple model. For ease with coding we will re-label the species and reaction rates to something less cumbersome. The parameter values and interpretation of the species labels is given in the code-blocks below. Using these we have\n",
    "\n",
    "\\begin{align}\n",
    "\\frac{dx_1}{dt} &= kc_rx_5 - kc_fx_1^{n_1}x_4^{n_2}\\nonumber \\\\\n",
    "\\frac{dx_2}{dt} &= kc_rx_3 - kc_fx_2^{n_3}x_6^{n_4}\\nonumber \\\\\n",
    "\\frac{dx_3}{dt} &= kc_fx_2^{n_3}x_6^{n_4} - kc_rx_3\\nonumber \\\\\n",
    "\\frac{dx_4}{dt} &= kc_rx_5 - kc_fx_1^{n_1}x_4^{n_2} + 10ko_1\\nonumber \\\\\n",
    "\\frac{dx_5}{dt} &= kc_fx_1^{n_1}x_4^{n_2} - kc_rx_5 - k_{d1}x_5\\nonumber \\\\\n",
    "\\frac{dx_6}{dt} &= kc_rx_3 - kc_fx_2^{n_3}x_6^{n_4} + 10x_8\\frac{\\frac{k_bko_{f1}}{ko_{r1}}n_r + \\frac{k_akao_f}{kao_r}n_r\\bigg(\\frac{ka_f}{ka_r}x_5\\bigg)^{n_5}}{1 + \\frac{ko_{f1}}{ko_{r1}}n_r + \\frac{kao_f}{kao_r}n_r\\bigg(\\frac{ka_f}{ka_r}x_5\\bigg)^{n_5}}\\nonumber \\\\\n",
    "\\frac{dx_7}{dt} &= 10x_8\\frac{\\frac{k_bko_{f1}}{ko_{r1}}n_r + \\frac{k_akao_f}{kao_r}n_r\\bigg(\\frac{ka_f}{ka_r}x_5\\bigg)^{n_5}}{1 + \\frac{ko_{f1}}{ko_{r1}}n_r + \\frac{kao_f}{kao_r}n_r\\bigg(\\frac{ka_f}{ka_r}x_5\\bigg)^{n_5}} + 10ko_2 - k_{d2}x_7\\nonumber \\\\\n",
    "\\frac{dx_8}{dt} &= 0\\nonumber \\\\\n",
    "\\frac{dx_9}{dt} &= 10x_{10}\\frac{ko_3\\frac{ko_{f2}}{ko_{r2}}n_r}{1+\\frac{ko_{f2}}{ko_{r2}}n_r+\\bigg(\\frac{kr_f}{kr_r}x_3\\bigg)^{n_6}} + 10ko_3 - k_{d3}x_9\\nonumber \\\\\n",
    "\\frac{dx_{10}}{dt} &= 0\\nonumber\n",
    "\\end{align}\n",
    "\n",
    "Given the parameter values and the initial conditions in the appendix tables, simulate the system from an initial timepoint of 0 to 1000 with 10000 timesteps. Note the initial Ara ($x_1$) concentration = 1.\n",
    "\n",
    "When coding the ODE model, we will define a vector of parameters to be\n",
    "\n",
    "parameters = ($kc_r,kc_f,ko_1,k_{d1},k_b,ko_{f1},ko_{r1},n_r,k_a,kao_f,kao_r,ka_f,ka_r,ko_2,k_{d2},ko_3,k_{d3},ko_{f2},ko_{r2},kr_f,kr_r,n_1,n_2,n_3,n_4,n_5,n_6$)\n",
    "\n",
    "and initial conditions that have been provided for you.\n",
    "\n",
    "Plot the resulting time-series of GFP. Does this match what you saw on iBioSim?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Specify parameter values\n",
    "kcr = 1\n",
    "kcf = 0.05\n",
    "ko1 = 0.05\n",
    "kd1 = 0.0075\n",
    "kb = 0.0001\n",
    "kof1 = 0.033\n",
    "kor1 = 1\n",
    "nr = 30\n",
    "ka = 0.25\n",
    "kaof = 1\n",
    "kaor = 1\n",
    "kaf = 0.033\n",
    "kar = 1\n",
    "ko2 = 0.05\n",
    "kd2 = 0.0075\n",
    "ko3 = 0.05\n",
    "kd3 = 0.0075\n",
    "kof2 = 0.033\n",
    "kor2 = 1\n",
    "krf = 0.5\n",
    "krr = 1\n",
    "n1 = 1\n",
    "n2 = 1\n",
    "n3 = 1\n",
    "n4 = 1\n",
    "n5 = 2\n",
    "n6 = 2\n",
    "\n",
    "# set up parameters vector\n",
    "parameters = [kcr, kcf, ko1, kd1, kb, kof1, kor1, nr, ka, kaof, kaor, kaf, kar, ko2, kd2, ko3, kd3, kof2, kor2, krf, krr, n1, n2, n3, n4, n5, n6]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set initial values for all state variables\n",
    "x1_init = 1 # x1 = Ara\n",
    "x2_init = 10 # x2 = dCas9\n",
    "x3_init = 0 # x3 = sgRNA-dCas9\n",
    "x4_init = 0 # x4 = AraC\n",
    "x5_init = 0 # x5 = AraComplex\n",
    "x6_init = 0 # x6 = sgRNA\n",
    "x7_init = 0 # x7 = mKO2\n",
    "x8_init = 2 # x8 = pBAD\n",
    "x9_init = 0 # x9 = GFP\n",
    "x10_init = 2 # x10 = pGFP\n",
    "\n",
    "# set up vector\n",
    "initial_conditions = [x1_init, x2_init, x3_init, x4_init, x5_init, x6_init, x7_init, x8_init, x9_init, x10_init]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Place your ODE here"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write a SimulateODE function here that outputs the timepoints and time-series of x_t"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write a plotODEoutput function here that plots the concentration of GFP (x_t[:,8])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Plot output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
